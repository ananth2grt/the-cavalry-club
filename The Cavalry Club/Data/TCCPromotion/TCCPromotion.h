//
//  TCCPromotion.h
//  The Cavalry Club
//
//  Created by Ananth on 02/07/16.
//  Copyright © 2016 AvidMindWorks. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, TCCPromotionType) {
    TCCPromotionTypeGeneral,
    TCCPromotionTypeBeverage,
    TCCPromotionTypeFood
};

@interface TCCPromotion : NSObject
@property (nonatomic) NSInteger ID;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *imagePath;
@property (nonatomic) BOOL status;
@end
