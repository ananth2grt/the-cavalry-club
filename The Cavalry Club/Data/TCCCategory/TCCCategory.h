//
//  TCCCategory.h
//  The Cavalry Club
//
//  Created by Ananth on 02/07/16.
//  Copyright © 2016 AvidMindWorks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TCCCategory : NSObject
@property (nonatomic) NSInteger ID;
@property (nonatomic, strong) NSString *name;
@property (nonatomic) BOOL hasChildren;
@property (nonatomic) BOOL isOpen;
@end
