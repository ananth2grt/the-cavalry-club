//
//  TCCCartItem.h
//  The Cavalry Club
//
//  Created by Ananth on 07/07/16.
//  Copyright © 2016 AvidMindWorks. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSInteger const TCCCartInitialValue;

@class TCCMenuItem;
@interface TCCCartItem : NSObject
@property (nonatomic, strong) TCCMenuItem *menuItem;
@property (nonatomic) NSInteger quantity;
@property (nonatomic) double total;
@end
