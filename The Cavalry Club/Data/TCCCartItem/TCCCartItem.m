//
//  TCCCartItem.m
//  The Cavalry Club
//
//  Created by Ananth on 07/07/16.
//  Copyright © 2016 AvidMindWorks. All rights reserved.
//

#import "TCCCartItem.h"
#import "TCCMenuItem.h"

NSInteger const TCCCartInitialValue = 1;

@implementation TCCCartItem

-(instancetype)init {
    if ([super init]) {
        _quantity = TCCCartInitialValue;
    }
    return self;
}

-(double)total {
    return _menuItem.price.floatValue * _quantity;
}

@end
