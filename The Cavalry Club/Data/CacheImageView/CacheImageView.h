//
//  CacheImageView.h
//  The Cavalry Club
//
//  Created by Ananth on 03/07/16.
//  Copyright © 2016 AvidMindWorks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CacheImageView : UIImageView
@property (nonatomic, strong) NSString *remotePath;
@end
