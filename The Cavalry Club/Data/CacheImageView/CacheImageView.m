//
//  CacheImageView.m
//  The Cavalry Club
//
//  Created by Ananth on 03/07/16.
//  Copyright © 2016 AvidMindWorks. All rights reserved.
//

#import "CacheImageView.h"
#import "NSString+Helper.h"

static NSString *const imagesCache = @"imagesCache";

@interface CacheImageView ()
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;
@end

@implementation CacheImageView

- (NSString*)localPath {
    NSString *cacheDirectory = [[NSString cachesDirectory] stringByAppendingPathComponent:imagesCache];
    if (![[NSFileManager defaultManager] fileExistsAtPath:cacheDirectory]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:cacheDirectory withIntermediateDirectories:NO attributes:nil error:nil];
    }    
    NSString *localPath = _remotePath ? [cacheDirectory stringByAppendingPathComponent:_remotePath.pathComponents.lastObject] : cacheDirectory;
    return localPath;
}

- (UIActivityIndicatorView*)activityIndicator {
    if (!_activityIndicator) {
        _activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        _activityIndicator.tintColor = [UIColor blueColor];
        _activityIndicator.hidesWhenStopped = YES;
        [self addSubview:_activityIndicator];
        _activityIndicator.frame = CGRectMake(0, 0, 50, 20);
        _activityIndicator.center = self.center;
    }
    return _activityIndicator;
}

-(void)setRemotePath:(NSString*)remotePath {
    _remotePath = remotePath;
    NSString *localPath = [self localPath];
    if (![[NSFileManager defaultManager] fileExistsAtPath:localPath]) {
        [self activityIndicator];
        [_activityIndicator startAnimating];
        [self downloadImage:^(NSData *data, NSError *error) {
            if (!error) {
                [data writeToFile:localPath atomically:YES];
            }
            self.image = [UIImage imageWithContentsOfFile:localPath];
            [_activityIndicator stopAnimating];
        }];
    }
    else {
        self.image = [UIImage imageWithContentsOfFile:localPath];
    }
}

- (void)downloadImage:(void(^)(NSData *data, NSError *error))completion {
    NSURLSession *session = [NSURLSession sharedSession];
    [[session dataTaskWithURL:[NSURL URLWithString:_remotePath] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        completion(data, error);
    }] resume];
}

@end
