//
//  TCCOrder.m
//  The Cavalry Club
//
//  Created by Ananth on 07/07/16.
//  Copyright © 2016 AvidMindWorks. All rights reserved.
//

#import "TCCOrder.h"

@implementation TCCOrder

+ (id)sharedInstance
{
    // structure used to test whether the block has completed or not
    static dispatch_once_t p = 0;
    
    // initialize sharedObject as nil (first call only)
    __strong static id _sharedObject = nil;
    
    // executes a block object once and only once for the lifetime of an application
    dispatch_once(&p, ^{
        _sharedObject = [[self alloc] init];
    });
    
    // returns the same object each time
    return _sharedObject;
}

-(instancetype)init {
    if (self = [super init]) {
        _cartItems = [NSMutableArray array];
    }
    return self;
}

-(void)clearOrder {
    [_cartItems removeAllObjects];
}

@end
