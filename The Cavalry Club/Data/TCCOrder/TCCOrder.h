//
//  TCCOrder.h
//  The Cavalry Club
//
//  Created by Ananth on 07/07/16.
//  Copyright © 2016 AvidMindWorks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TCCOrder : NSObject
+(id)sharedInstance;
-(void)clearOrder;
@property (nonatomic, strong) NSMutableArray *cartItems;
@end
