//
//  TCCMenuItem.h
//  The Cavalry Club
//
//  Created by Ananth on 02/07/16.
//  Copyright © 2016 AvidMindWorks. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, MenuType) {
    MenuTypeBeverage,
    MenuTypeFood
};

extern NSInteger const MenuItemAllIDX;

@interface TCCMenuItem : NSObject
@property (nonatomic) NSInteger ID;
@property (nonatomic, strong) NSString *category;
@property (nonatomic, strong) NSString *item;
@property (nonatomic, strong) NSString *cusine;
@property (nonatomic, strong) NSString *ingredient;
@property (nonatomic, strong) NSString *descriptionText;
@property (nonatomic, strong) NSString *price;
@property (nonatomic, strong) NSString *imagePath;
@property (nonatomic) MenuType menuType;

+ (TCCMenuItem*)menuItemAll:(MenuType)menuType;

@end
