//
//  TCCMenuItem.m
//  The Cavalry Club
//
//  Created by Ananth on 02/07/16.
//  Copyright © 2016 AvidMindWorks. All rights reserved.
//

#import "TCCMenuItem.h"

NSInteger const MenuItemAllIDX = -1;

@implementation TCCMenuItem

+(TCCMenuItem*)menuItemAll:(MenuType)menuType {
    TCCMenuItem *menuItem = [self new];
    menuItem.menuType = menuType;
    menuItem.ID = MenuItemAllIDX;
    menuItem.category = [[ConfigurationHelper sharedInstance] appLocalizedStringForKey:@"ALL"];
    return menuItem;
}

@end
