//
//  TCCAPICommunicator.h
//  The Cavalry Club
//
//  Created by Ananth on 02/07/16.
//  Copyright © 2016 AvidMindWorks. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TCCDisplayText.h"
#import "TCCPromotion.h"

@interface TCCAPICommunicator : NSObject
- (void)getFoods:(void(^)(NSArray *response, NSError *error))completion;
- (void)getBeverages:(void(^)(NSArray *response, NSError *error))completion;
- (void)getPromotionsOfType:(TCCPromotionType)type completion:(void(^)(NSArray *response, NSError *error))completion;
- (void)getDisplayTextForScreen:(TCCDisplayTextScreen)screenType completion:(void (^)(TCCDisplayText *tccDisplayText, NSError *error))completion;
- (void)syncRemoteData:(void(^)(NSError *error))completion;
+ (id)sharedInstance;
@end
