//
//  TCCAPICommunicator.m
//  The Cavalry Club
//
//  Created by Ananth on 02/07/16.
//  Copyright © 2016 AvidMindWorks. All rights reserved.
//

#import "TCCAPICommunicator.h"
#import "TCCMenuItem.h"

#import "ConfigurationHelper.h"
#import "NSString+Helper.h"

@implementation TCCAPICommunicator

+ (id)sharedInstance
{
    // structure used to test whether the block has completed or not
    static dispatch_once_t p = 0;
    
    // initialize sharedObject as nil (first call only)
    __strong static id _sharedObject = nil;
    
    // executes a block object once and only once for the lifetime of an application
    dispatch_once(&p, ^{
        _sharedObject = [[self alloc] init];
    });
    
    // returns the same object each time
    return _sharedObject;
}

- (void)syncRemoteData:(void(^)(NSError *error))completion {
    [self fetchDisplayTextForScreen:TCCDisplayTextScreenWelcome completion:^(TCCDisplayText *tccDisplayText, NSError *error) {
        DLog(@"fetchDisplayTextForScreen for: %ld", (long)TCCDisplayTextScreenWelcome);
    }];
    [self fetchDisplayTextForScreen:TCCDisplayTextScreenFoods completion:^(TCCDisplayText *tccDisplayText, NSError *error) {
        DLog(@"fetchDisplayTextForScreen for: %ld", (long)TCCDisplayTextScreenFoods);
    }];
    [self fetchBeverages:^(NSArray *response, NSError *error) {
        DLog(@"fetchBeverages");
    }];
    [self fetchFoods:^(NSArray *response, NSError *error) {
        DLog(@"fetchFoods");
    }];
    
    NSArray *tccPromotionTypes = @[@(TCCPromotionTypeGeneral), @(TCCPromotionTypeBeverage), @(TCCPromotionTypeFood)];
    [tccPromotionTypes enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [self fetchPromotionsOfType:[obj integerValue] completion:^(NSArray *response, NSError *error) {
            DLog(@"fetchPromotionsOfType: %d", [obj integerValue]);
        }];
    }];
}

#pragma mark - Remote fetches

-(void)fetchDisplayTextForScreen:(TCCDisplayTextScreen)screenType completion:(void(^)(TCCDisplayText *tccDisplayText, NSError *error))completion {
    __weak typeof(self) weakSelf = self;
    NSURLSession *session = [NSURLSession sharedSession];
    NSString *apiURL = screenType == TCCDisplayTextScreenWelcome ? welcomeTextAPI : foodTextAPI;
    [[session dataTaskWithURL:[NSURL URLWithString:apiURL] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (data) {
            NSDictionary *webResponse = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
            NSArray *rawDisplayText = [webResponse valueForKeyPath:responseDataKey];
            if (rawDisplayText.count) {
                NSString *localFile = [weakSelf fileForSavedResponse:[apiURL stringByAppendingPathComponent:(screenType == TCCDisplayTextScreenWelcome ? @"WelcomeText" : @"FoodText")]];
                [weakSelf saveResponse:rawDisplayText toFile:localFile];
                NSArray *models = [weakSelf processResponse:rawDisplayText forClass:[TCCDisplayText class]];
                completion(models.firstObject, nil);
            }
        }
        else {
            NSLog(@"Error getting Welcome text");
            NSError *fetchError = error ? : [NSError errorWithDomain:@"com.cavalry.club" code:0 userInfo:@{@"reason":@"data cannot be null"}];
            completion(nil, fetchError);
        }
    }] resume];
}

- (void)fetchBeverages:(void(^)(NSArray *response, NSError *error))completion {
    __weak typeof(self) weakSelf = self;
    NSURLSession *session = [NSURLSession sharedSession];
    [[session dataTaskWithURL:[NSURL URLWithString:beverageAPI] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (data) {
            NSDictionary *webResponse = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
            NSArray *rawBeverages = [webResponse valueForKeyPath:responseDataKey];
            if (rawBeverages.count) {
                NSString *localFile = [weakSelf fileForSavedResponse:beverageAPI];
                [weakSelf saveResponse:rawBeverages toFile:localFile];
                NSArray *menuModels = [weakSelf processMenuResponse:rawBeverages menuType:MenuTypeBeverage];
                completion(menuModels, nil);
            }
        }
        else {
            NSLog(@"Error getting beverages");
            NSError *fetchError = error ? : [NSError errorWithDomain:@"com.cavalry.club" code:0 userInfo:@{@"reason":@"data cannot be null"}];
            completion(nil, fetchError);
        }
    }] resume];
}

- (void)fetchFoods:(void(^)(NSArray *response, NSError *error))completion {
    __weak typeof(self) weakSelf = self;
    NSURLSession *session = [NSURLSession sharedSession];
    [[session dataTaskWithURL:[NSURL URLWithString:foodAPI] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (data) {
            NSDictionary *webResponse = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
            NSArray *rawFoods = [webResponse valueForKeyPath:responseDataKey];
            if (rawFoods.count) {
                NSString *localFile = [weakSelf fileForSavedResponse:foodAPI];
                [weakSelf saveResponse:rawFoods toFile:localFile];
                NSArray *menuModels = [weakSelf processMenuResponse:rawFoods menuType:MenuTypeFood];
                completion(menuModels, nil);
            }
        }
        else {
            NSLog(@"Error getting foods");
            NSError *fetchError = error ? : [NSError errorWithDomain:@"com.cavalry.club" code:0 userInfo:@{@"reason":@"data cannot be null"}];
            completion(nil, fetchError);
        }
    }] resume];
}

- (void)fetchPromotionsOfType:(TCCPromotionType)type completion:(void(^)(NSArray *response, NSError *error))completion {
    __weak typeof(self) weakSelf = self;
    NSURLSession *session = [NSURLSession sharedSession];
    NSString *respectiveAPI = [self APIForTCCPromotionType:type];
    [[session dataTaskWithURL:[NSURL URLWithString:respectiveAPI] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (data) {
            NSDictionary *webResponse = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
            NSArray *rawPromotions = [webResponse valueForKeyPath:responseDataKey];
            if (rawPromotions.count) {
                NSString *localFile = [weakSelf fileForSavedResponse:respectiveAPI];
                [weakSelf saveResponse:rawPromotions toFile:localFile];
                NSArray *promotionModels = [weakSelf processResponse:rawPromotions forClass:[TCCPromotion class]];
                completion(promotionModels, nil);
            }
        }
        else {
            NSLog(@"Error getting promotions");
            NSError *fetchError = error ? : [NSError errorWithDomain:@"com.cavalry.club" code:0 userInfo:@{@"reason":@"data cannot be null"}];
            completion(nil, fetchError);
        }
    }] resume];
}

#pragma mark - Model Helpers

- (NSArray*)processResponse:(NSArray*)response forClass:(Class)class {
    __block NSMutableArray *models = [NSMutableArray array];
    if (class == [TCCPromotion class]) {
        NSArray *apiPromotionsKeys = @[@"id", @"Title", @"Imagepath", @"status"];
        [response enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            BOOL status = [[obj objectForKey:apiPromotionsKeys[3]] boolValue];
            if (status) {
                TCCPromotion *promotions = [TCCPromotion new];
                promotions.ID = [[obj objectForKey:apiPromotionsKeys[0]] integerValue];
                promotions.title = [obj objectForKey:apiPromotionsKeys[1]];
                promotions.imagePath = [obj objectForKey:apiPromotionsKeys[2]];
                promotions.status = status;
                [models addObject:promotions];
            }
        }];
    }
    else if (class == [TCCDisplayText class]) {
        NSArray *engMenuKeys = @[@"id", @"title", @"content"];
        NSArray *otherMenuKeys = @[@"id", @"title_arabic", @"content_arabic"];;
        NSArray *apiMenuKeys = [[ConfigurationHelper sharedInstance] selectedLanguage] == AppLanguageEnglish ? engMenuKeys : otherMenuKeys;
        [response enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            TCCDisplayText *tccDisplayText = [TCCDisplayText new];
            tccDisplayText.ID = [[obj objectForKey:apiMenuKeys[0]] integerValue];
            tccDisplayText.title = [obj objectForKey:apiMenuKeys[1]];
            tccDisplayText.content = [obj objectForKey:apiMenuKeys[2]];
            [models addObject:tccDisplayText];
        }];
    }
    return models;
}

- (NSArray*)processMenuResponse:(NSArray*)response menuType:(MenuType)menuType {
    // get keys based on the language
    static NSArray *engMenuKeys = nil;
    if (!engMenuKeys) {
        engMenuKeys = @[@"id", @"category_eng", @"item_eng", @"cusine_eng", @"ingredient_eng", @"desc_eng", @"price", @"imagepath"];
    }
    static NSArray *otherMenuKeys = nil;
    if (!otherMenuKeys) {
        otherMenuKeys = @[@"id", @"category_other", @"item_other", @"cusine_other", @"ingredient_other", @"desc_other", @"price", @"imagepath"];
    }
    
    // initialize with "MenuItemAll"
    __block NSMutableArray *menuItems = [NSMutableArray arrayWithObject:[TCCMenuItem menuItemAll:menuType]];
    NSArray *apiMenuKeys = [[ConfigurationHelper sharedInstance] selectedLanguage] == AppLanguageEnglish ? engMenuKeys : otherMenuKeys;
    [response enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        TCCMenuItem *menu = [TCCMenuItem new];
        menu.ID = [[obj objectForKey:apiMenuKeys[0]] integerValue];
        menu.category = [obj objectForKey:apiMenuKeys[1]];
        menu.item = [obj objectForKey:apiMenuKeys[2]];
        menu.cusine = [obj objectForKey:apiMenuKeys[3]];
        menu.ingredient = [obj objectForKey:apiMenuKeys[4]];
        menu.descriptionText = [obj objectForKey:apiMenuKeys[5]];
        menu.price = [obj objectForKey:apiMenuKeys[6]];
        menu.imagePath = [obj objectForKey:apiMenuKeys[7]];
        menu.menuType = menuType;
        [menuItems addObject:menu];
    }];
    return menuItems;
}

#pragma mark - File Helpers

- (NSString*)fileForSavedResponse:(NSString*)url {
    return [[NSString documentsDirectory] stringByAppendingPathComponent:url.lastPathComponent.stringByDeletingPathExtension];
}

- (void)saveResponse:(id)anObject toFile:(NSString*)file {
    [anObject writeToFile:file atomically:YES];
}

#pragma mark - Local fetches

-(void)getBeverages:(void(^)(NSArray *response, NSError *error))completion {
    NSString *localFile = [self fileForSavedResponse:beverageAPI];
    if ([[NSFileManager defaultManager] fileExistsAtPath:localFile]) {
        NSArray *rawBeverages = [NSArray arrayWithContentsOfFile:localFile];
        NSArray *menuModels = [self processMenuResponse:rawBeverages menuType:MenuTypeBeverage];
        completion(menuModels, nil);
    }
    else {
        [self fetchBeverages:^(NSArray *response, NSError *error) {
            completion(response, error);
        }];
    }
}

-(void)getFoods:(void(^)(NSArray *response, NSError *error))completion {
    NSString *localFile = [self fileForSavedResponse:foodAPI];
    if ([[NSFileManager defaultManager] fileExistsAtPath:localFile]) {
        NSArray *rawFoods = [NSArray arrayWithContentsOfFile:localFile];
        NSArray *menuModels = [self processMenuResponse:rawFoods menuType:MenuTypeFood];
        completion(menuModels, nil);
    }
    else {
        [self fetchFoods:^(NSArray *response, NSError *error) {
            completion(response, error);
        }];
    }
}

- (NSString*)APIForTCCPromotionType:(TCCPromotionType)type {
    NSString *respectiveAPI = nil;
    switch (type) {
        case TCCPromotionTypeGeneral:
            respectiveAPI = promotionsGeneralAPI;
            break;
        case TCCPromotionTypeFood:
            respectiveAPI = promotionsFoodAPI;
            break;
        case TCCPromotionTypeBeverage:
            respectiveAPI = promotionsBeverageAPI;
            break;
        default:
            break;
    }
    return respectiveAPI;
}

-(void)getPromotionsOfType:(TCCPromotionType)type completion:(void(^)(NSArray *response, NSError *error))completion {
    NSString *respectiveAPI = [self APIForTCCPromotionType:type];
    NSString *localFile = [self fileForSavedResponse:respectiveAPI];
    if ([[NSFileManager defaultManager] fileExistsAtPath:localFile]) {
        NSArray *rawPromotions = [NSArray arrayWithContentsOfFile:localFile];
        NSArray *promotionModels = [self processResponse:rawPromotions forClass:[TCCPromotion class]];
        completion(promotionModels, nil);
    }
    else {
        [self fetchPromotionsOfType:(TCCPromotionType)type completion:^(NSArray *response, NSError *error) {
            completion(response, error);
        }];
    }
}

-(void)getDisplayTextForScreen:(TCCDisplayTextScreen)screenType completion:(void(^)(TCCDisplayText *tccDisplayText, NSError *error))completion {
    NSString *apiURL = (screenType == TCCDisplayTextScreenWelcome) ? welcomeTextAPI : foodTextAPI;
    NSString *localFile = [self fileForSavedResponse:[apiURL stringByAppendingPathComponent:(screenType == TCCDisplayTextScreenWelcome ? @"WelcomeText" : @"FoodText")]];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:localFile]) {
        NSArray *rawDisplayText = [NSArray arrayWithContentsOfFile:localFile];
        NSArray *models = [self processResponse:rawDisplayText forClass:[TCCDisplayText class]];
        completion(models.firstObject, nil);
    }
    else {
        [self fetchDisplayTextForScreen:screenType completion:^(TCCDisplayText *tccDisplayText, NSError *error) {
            completion(tccDisplayText, error);
        }];
    }
}

@end
