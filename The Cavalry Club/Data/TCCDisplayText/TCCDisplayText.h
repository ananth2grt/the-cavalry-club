//
//  TCCDisplayText.h
//  The Cavalry Club
//
//  Created by Ananth on 24/07/16.
//  Copyright © 2016 AvidMindWorks. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, TCCDisplayTextScreen) {
    TCCDisplayTextScreenWelcome,
    TCCDisplayTextScreenFoods
};

@interface TCCDisplayText : NSObject
@property (nonatomic) NSInteger ID;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *content;
@property (nonatomic) TCCDisplayTextScreen screenType;
@end
