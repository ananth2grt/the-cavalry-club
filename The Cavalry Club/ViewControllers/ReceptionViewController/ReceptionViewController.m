//
//  ReceptionViewController.m
//  The Cavalry Club
//
//  Created by Ananth on 23/07/16.
//  Copyright © 2016 AvidMindWorks. All rights reserved.
//

#import "ReceptionViewController.h"
#import "TCCAPICommunicator.h"

@interface ReceptionViewController () {
    __weak IBOutlet UILabel *_txtLabel;
    __weak IBOutlet UILabel *_contentLabel;
}
@property (nonatomic, strong) TCCDisplayText *tccDisplayText;
@end

@implementation ReceptionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)setDisplayTextScreenType:(TCCDisplayTextScreen)displayTextScreenType {
    _displayTextScreenType = displayTextScreenType;
    __weak typeof(self) weakSelf = self;
    [[TCCAPICommunicator sharedInstance] getDisplayTextForScreen:_displayTextScreenType completion:^(TCCDisplayText *tccDisplayText, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            weakSelf.tccDisplayText = tccDisplayText;
        });
    }];
}

- (void)setValues {
    __weak typeof(self) weakSelf = self;
    _txtLabel.font = [UIFont fontWithName:[[ConfigurationHelper sharedInstance] appFont] size:28.0];
    _contentLabel.font = [UIFont fontWithName:[[ConfigurationHelper sharedInstance] appFont] size:24.0];
    _txtLabel.attributedText = _tccDisplayText.title ? [weakSelf underLinedAttributedText:_tccDisplayText.title] : nil;
    _contentLabel.text = [_tccDisplayText.content stringByReplacingOccurrencesOfString:@"<br/>" withString:@"\n"];
    _contentLabel.textAlignment = [[ConfigurationHelper sharedInstance] selectedLanguage] == AppLanguageArabic ? NSTextAlignmentRight : NSTextAlignmentLeft;
}

-(void)setTccDisplayText:(TCCDisplayText*)tccDisplayText {
    _tccDisplayText = tccDisplayText;
    [self setValues];
}

- (NSAttributedString*)underLinedAttributedText:(NSString*)text {
    NSDictionary *underlineAttribute = @{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)};
    return [[NSAttributedString alloc] initWithString:text attributes:underlineAttribute];
}

@end
