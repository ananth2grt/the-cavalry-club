//
//  ReceptionViewController.h
//  The Cavalry Club
//
//  Created by Ananth on 23/07/16.
//  Copyright © 2016 AvidMindWorks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TCCDisplayText.h"

@interface ReceptionViewController : UIViewController
@property (nonatomic) TCCDisplayTextScreen displayTextScreenType;
@end
