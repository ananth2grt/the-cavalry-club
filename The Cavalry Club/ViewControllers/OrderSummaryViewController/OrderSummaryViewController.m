//
//  OrderSummaryViewController.m
//  The Cavalry Club
//
//  Created by Ananth on 07/07/16.
//  Copyright © 2016 AvidMindWorks. All rights reserved.
//

#import "OrderSummaryViewController.h"
#import "OrderSummaryCell.h"
#import "TCCOrder.h"
#import "TCCCartItem.h"

@interface OrderSummaryViewController () <UIAlertViewDelegate> {
    __weak IBOutlet UITableView *_tableView;
    __weak IBOutlet UILabel *_lblOrderSummary;
    __weak IBOutlet UILabel *_lblAmount;
    __weak IBOutlet UIButton *_btnReset;
    __weak IBOutlet UILabel *_lblPrice;
    __weak IBOutlet UILabel *_lblTotalPrice;
    __weak IBOutlet UILabel *_lblItem;
    __weak IBOutlet UILabel *_lblQuantity;
    __weak IBOutlet UILabel *_lblAmountTitle;
}
@end

@implementation OrderSummaryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self localizeControls];
}

- (void)localizeControls {
    [_btnReset setTitle:[[ConfigurationHelper sharedInstance] appLocalizedStringForKey:@"RESET ORDER SUMMARY"] forState:UIControlStateNormal];
    _lblOrderSummary.text = [[ConfigurationHelper sharedInstance] appLocalizedStringForKey:@"ORDER SUMMARY"];
    _lblTotalPrice.text = [[ConfigurationHelper sharedInstance] appLocalizedStringForKey:@"TOTAL PRICE"];
    _lblItem.text = [[ConfigurationHelper sharedInstance] appLocalizedStringForKey:@"ITEM"];
    _lblQuantity.text = [[ConfigurationHelper sharedInstance] appLocalizedStringForKey:@"QUANTITY"];
    _lblPrice.text = [[ConfigurationHelper sharedInstance] appLocalizedStringForKey:@"PRICE"];
    _lblAmountTitle.text = [[ConfigurationHelper sharedInstance] appLocalizedStringForKey:@"AMOUNT"];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self refreshValues];
}

- (void)refreshValues {
    [_tableView reloadData];
    [self setFinalAmount];
}

- (void)setFinalAmount {
    __block double total = 0;
    [[[TCCOrder sharedInstance] cartItems] enumerateObjectsUsingBlock:^(TCCCartItem *obj, NSUInteger idx, BOOL *stop) {
        total += obj.total;
    }];
    _lblAmount.text = [NSString stringWithFormat:@"%@ %.2f", currencySymbol, total];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)btnResetOrderTapped:(id)sender {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[[ConfigurationHelper sharedInstance] appLocalizedStringForKey:@"Alert"] message:[[ConfigurationHelper sharedInstance] appLocalizedStringForKey:@"Are you sure to reset the order ?"] delegate:self cancelButtonTitle:[[ConfigurationHelper sharedInstance] appLocalizedStringForKey:@"Cancel"] otherButtonTitles:[[ConfigurationHelper sharedInstance] appLocalizedStringForKey:@"Ok"], nil];
    alert.tag = kAlertView;
    [alert show];
}

#pragma mark <AlertView>

-(void)alertView:(UIAlertView*)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    [alertView dismissWithClickedButtonIndex:0 animated:NO];
    if (buttonIndex) {
        if (alertView.tag) {
            [[TCCOrder sharedInstance] clearOrder];
            [self refreshValues];
 // let know the parent that reset is selected
            if (_delegate && [_delegate respondsToSelector:@selector(didSelectReset:)]) {
                [_delegate didSelectReset:self];
            }
        }
    }
}

#pragma mark <Table View>

-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath {
    OrderSummaryCell *orderSummaryCell = [tableView dequeueReusableCellWithIdentifier:[OrderSummaryCell cellID] forIndexPath:indexPath];
    orderSummaryCell.cartItem = [[TCCOrder sharedInstance] cartItems][indexPath.row];
    return orderSummaryCell;
}

-(NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section {
    return [[TCCOrder sharedInstance] cartItems].count;
}


@end
