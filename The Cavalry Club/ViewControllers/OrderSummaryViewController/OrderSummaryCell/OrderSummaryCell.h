//
//  OrderSummaryCell.h
//  The Cavalry Club
//
//  Created by Ananth on 07/07/16.
//  Copyright © 2016 AvidMindWorks. All rights reserved.
//

#import <UIKit/UIKit.h>
@class TCCCartItem;

@interface OrderSummaryCell : UITableViewCell
@property (nonatomic, strong) TCCCartItem *cartItem;
+(NSString*)cellID;
@end
