//
//  OrderSummaryCell.m
//  The Cavalry Club
//
//  Created by Ananth on 07/07/16.
//  Copyright © 2016 AvidMindWorks. All rights reserved.
//

#import "OrderSummaryCell.h"
#import "TCCCartItem.h"
#import "TCCMenuItem.h"

@interface OrderSummaryCell ()
@property (weak, nonatomic) IBOutlet UILabel *lblQuantity;
@property (weak, nonatomic) IBOutlet UILabel *lblAmount;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblPrice;
@end

@implementation OrderSummaryCell

-(void)setCartItem:(TCCCartItem*)cartItem {
    _cartItem = cartItem;
    _lblTitle.text = _cartItem.menuItem.item;
    _lblQuantity.text = [NSString stringWithFormat:@"%ld", (long)_cartItem.quantity];
    _lblAmount.text = [NSString stringWithFormat:@"%@ %.2f", currencySymbol, _cartItem.total];
    _lblPrice.text = [NSString stringWithFormat:@"%@ %.2f", currencySymbol, _cartItem.menuItem.price.floatValue];
    self.contentView.backgroundColor = [UIColor clearColor];
    self.backgroundColor = [UIColor clearColor];
}

+(NSString*)cellID {
    return @"OrderSummaryCell";
}

@end
