//
//  OrderSummaryViewController.h
//  The Cavalry Club
//
//  Created by Ananth on 07/07/16.
//  Copyright © 2016 AvidMindWorks. All rights reserved.
//

#import <UIKit/UIKit.h>
@class OrderSummaryViewController;
@protocol OrderSummaryDelegate <NSObject>
- (void)didSelectReset:(OrderSummaryViewController*)sender;
@end

@interface OrderSummaryViewController : UIViewController
@property (nonatomic, assign) id<OrderSummaryDelegate> delegate;
@end
