//
//  PromotionsDisplayViewController.h
//  The Cavalry Club
//
//  Created by Ananth on 07/07/16.
//  Copyright © 2016 AvidMindWorks. All rights reserved.
//

#import "TCCCollectionViewController.h"

@interface PromotionsDisplayViewController : TCCCollectionViewController
@property (nonatomic, strong) NSArray *promotions;
@end
