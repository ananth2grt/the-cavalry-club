//
//  PromotionsDisplayCell.m
//  The Cavalry Club
//
//  Created by Ananth on 07/07/16.
//  Copyright © 2016 AvidMindWorks. All rights reserved.
//

#import "PromotionsDisplayCell.h"
#import "CacheImageView.h"
#import "TCCPromotion.h"

@interface PromotionsDisplayCell ()
@property (weak, nonatomic) IBOutlet CacheImageView *imageView;
@end

@implementation PromotionsDisplayCell

+(NSString*)cellID {
    return @"PromotionsDisplayCell";
}

-(void)setPromotion:(TCCPromotion*)promotion {
    _promotion = promotion;
    _imageView.remotePath = [imagePathAPI stringByAppendingPathComponent:_promotion.imagePath];
}

@end
