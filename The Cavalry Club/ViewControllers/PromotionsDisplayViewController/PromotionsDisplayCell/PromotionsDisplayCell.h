//
//  PromotionsDisplayCell.h
//  The Cavalry Club
//
//  Created by Ananth on 07/07/16.
//  Copyright © 2016 AvidMindWorks. All rights reserved.
//

#import <UIKit/UIKit.h>
@class TCCPromotion;

@interface PromotionsDisplayCell : UICollectionViewCell
@property (nonatomic, strong) TCCPromotion *promotion;
+(NSString*)cellID;
@end
