//
//  PromotionsDisplayViewController.m
//  The Cavalry Club
//
//  Created by Ananth on 07/07/16.
//  Copyright © 2016 AvidMindWorks. All rights reserved.
//

#import "PromotionsDisplayViewController.h"
#import "PromotionsDisplayCell.h"

@implementation PromotionsDisplayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)setPromotions:(NSArray *)promotions {
    _promotions = promotions;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.collectionView reloadData];
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _promotions.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    PromotionsDisplayCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[PromotionsDisplayCell cellID] forIndexPath:indexPath];
    cell.promotion = _promotions[indexPath.row];
    return cell;
}

@end
