//
//  MenuDetailViewController.m
//  The Cavalry Club
//
//  Created by Ananth on 07/07/16.
//  Copyright © 2016 AvidMindWorks. All rights reserved.
//

#import "MenuDetailViewController.h"
#import "OrderDetailViewController.h"

#import "CacheImageView.h"
#import "TCCMenuItem.h"
#import "TCCOrder.h"
#import "TCCCartItem.h"

static NSString *const popOverSegue = @"popOverSegue";
static const float kLblHeight = 23.;
static const float kLblTopSpace = 14.;

@interface MenuDetailViewController () <UIAdaptivePresentationControllerDelegate, UIPopoverPresentationControllerDelegate> {
    __weak IBOutlet UILabel *_lblIngredients;
    __weak IBOutlet UILabel *_lblIngredientsTitle;
    __weak IBOutlet UILabel *_lblCusine;
    __weak IBOutlet UILabel *_lblPriceTitle;
    __weak IBOutlet UILabel *_lblCategory;
    __weak IBOutlet UILabel *_lblCategoryTitle;
    __weak IBOutlet UILabel *_lblPrice;
    __weak IBOutlet UILabel *_lblCusineTitle;
    __weak IBOutlet UITextView *_txtFldDescription;
    __weak IBOutlet UILabel *_lblDescriptionTitle;
    __weak IBOutlet CacheImageView *_imageView;
    __weak IBOutlet UILabel *_lblTitle;
    __weak IBOutlet NSLayoutConstraint *_lblIngredientsTitleTopConstraint;
    __weak IBOutlet NSLayoutConstraint *_lblIngredientsTitleHeightConstraint;
    __weak IBOutlet NSLayoutConstraint *_lblIngredientsHeightConstraint;
    __weak IBOutlet UIButton *_btnAddToCart;
}
@end

@implementation MenuDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setValues];
}

-(void)setMenuItem:(TCCMenuItem*)menuItem {
    _menuItem = menuItem;
}

- (void)setValues {
    //set values
    BOOL isBeverage = _menuItem.menuType == MenuTypeBeverage;
    BOOL isArabic = [[ConfigurationHelper sharedInstance] selectedLanguage] == AppLanguageArabic;
    _imageView.remotePath = [imagePathAPI stringByAppendingPathComponent:_menuItem.imagePath];
    [_btnAddToCart setTitle:[[ConfigurationHelper sharedInstance] appLocalizedStringForKey:@"ADD TO CART"] forState:UIControlStateNormal];
    // setting description lables on the right and values on the left for arabic
    if (isArabic) {
        _lblPriceTitle.text = [NSString stringWithFormat:@"%@ %@", currencySymbol, _menuItem.price];
        _lblCategoryTitle.text = _menuItem.category;
        _lblCusineTitle.text = _menuItem.cusine;
        _lblIngredientsTitle.text = _menuItem.ingredient;
        
        _lblCusine.text = isBeverage ? [[ConfigurationHelper sharedInstance] appLocalizedStringForKey:@"Origin:"] : [[ConfigurationHelper sharedInstance] appLocalizedStringForKey:@"Cusine:"];
        _lblPrice.text = [[ConfigurationHelper sharedInstance] appLocalizedStringForKey:@"Price:"];
        _lblCategory.text = [[ConfigurationHelper sharedInstance] appLocalizedStringForKey:@"Category:"];
        _lblIngredients.text = [[ConfigurationHelper sharedInstance] appLocalizedStringForKey:@"Ingredient:"];
    }
    else {
        _lblPrice.text = [NSString stringWithFormat:@"%@ %@", currencySymbol, _menuItem.price];
        _lblCategory.text = _menuItem.category;
        _lblCusine.text = _menuItem.cusine;
        _lblIngredients.text = _menuItem.ingredient;
        
        _lblCusineTitle.text = isBeverage ? [[ConfigurationHelper sharedInstance] appLocalizedStringForKey:@"Origin:"] : [[ConfigurationHelper sharedInstance] appLocalizedStringForKey:@"Cusine:"];
        _lblPriceTitle.text = [[ConfigurationHelper sharedInstance] appLocalizedStringForKey:@"Price:"];
        _lblCategoryTitle.text = [[ConfigurationHelper sharedInstance] appLocalizedStringForKey:@"Category:"];
        _lblIngredientsTitle.text = [[ConfigurationHelper sharedInstance] appLocalizedStringForKey:@"Ingredient:"];
    }
    _lblDescriptionTitle.text = [[ConfigurationHelper sharedInstance] appLocalizedStringForKey:@"Description:"];
    _lblDescriptionTitle.textAlignment = isArabic ? NSTextAlignmentRight : NSTextAlignmentLeft;
    _txtFldDescription.text = _menuItem.descriptionText;
    _txtFldDescription.selectable = NO;
    _lblTitle.text = _menuItem.item;

    _lblIngredientsHeightConstraint.constant = _lblIngredientsTitleHeightConstraint.constant = isBeverage ? 0 : kLblHeight;
    _lblIngredientsTitleTopConstraint.constant = isBeverage ? 0 : kLblTopSpace;
}

#pragma mark - Button Actions

- (IBAction)btnCloseTapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - PopOver

-(void)prepareForSegue:(UIStoryboardSegue*)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:popOverSegue]) {
        OrderDetailViewController * orderDetailViewController = (OrderDetailViewController*)segue.destinationViewController;
        orderDetailViewController.menuItem = _menuItem;
        
        UIPopoverPresentationController *popPC = orderDetailViewController.popoverPresentationController;
        popPC.sourceRect = orderDetailViewController.view.frame;
        popPC.delegate = self;
    }
}

-(UIModalPresentationStyle)adaptivePresentationStyleForPresentationController:(UIPresentationController*)controller {
    return UIModalPresentationCustom;
}

@end
