//
//  PromotionsViewController.h
//  The Cavalry Club
//
//  Created by Ananth on 02/07/16.
//  Copyright © 2016 AvidMindWorks. All rights reserved.
//

#import "TCCCollectionViewController.h"

@interface PromotionsViewController : TCCCollectionViewController
@property (nonatomic, strong) NSArray *promotions;
@end
