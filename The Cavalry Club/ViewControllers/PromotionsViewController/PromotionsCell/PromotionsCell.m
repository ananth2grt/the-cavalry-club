//
//  PromotionsCell.m
//  The Cavalry Club
//
//  Created by Ananth on 02/07/16.
//  Copyright © 2016 AvidMindWorks. All rights reserved.
//

#import "PromotionsCell.h"
#import "CacheImageView.h"
#import "TCCPromotion.h"

@interface PromotionsCell ()
@property (weak, nonatomic) IBOutlet CacheImageView *imageView;
@end

@implementation PromotionsCell

+ (NSString*)cellID {
    return @"PromotionsCell";
}

-(void)setPromotion:(TCCPromotion*)promotion {
    _promotion = promotion;
    _imageView.remotePath = [imagePathAPI stringByAppendingPathComponent:_promotion.imagePath];
}

@end
