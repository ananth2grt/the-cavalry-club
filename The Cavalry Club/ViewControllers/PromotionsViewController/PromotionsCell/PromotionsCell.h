//
//  PromotionsCell.h
//  The Cavalry Club
//
//  Created by Ananth on 02/07/16.
//  Copyright © 2016 AvidMindWorks. All rights reserved.
//

#import <UIKit/UIKit.h>
@class TCCPromotion;
@interface PromotionsCell : UICollectionViewCell
@property (nonatomic, strong) TCCPromotion *promotion;
+ (NSString*)cellID;
@end
