//
//  OrderDetailViewController.h
//  The Cavalry Club
//
//  Created by Ananth on 07/07/16.
//  Copyright © 2016 AvidMindWorks. All rights reserved.
//

#import <UIKit/UIKit.h>
@class TCCMenuItem;

@interface OrderDetailViewController : UIViewController
@property (nonatomic, strong) TCCMenuItem *menuItem;
@end
