//
//  OrderDetailViewController.m
//  The Cavalry Club
//
//  Created by Ananth on 07/07/16.
//  Copyright © 2016 AvidMindWorks. All rights reserved.
//

#import "OrderDetailViewController.h"
#import "TCCCartItem.h"
#import "TCCMenuItem.h"
#import "TCCOrder.h"

@interface OrderDetailViewController () {
    __weak IBOutlet UILabel *_lblTitle;
    __weak IBOutlet UILabel *_lblPrice;
    __weak IBOutlet UILabel *_lblQuantity;
    __weak IBOutlet UILabel *_lblTotal;
    __weak IBOutlet UIStepper *_stepper;
    NSInteger _initialQuantity;
    __weak IBOutlet UIButton *_btnCancel;
    __weak IBOutlet UILabel *_lblTotalTitle;
    __weak IBOutlet UIButton *_btnSave;
    __weak IBOutlet UILabel *_lblQuantityTitle;
    __weak IBOutlet UILabel *_lblPriceTitle;
    BOOL _addedToCart;
}
@property (nonatomic, strong) TCCCartItem *cartItem;
@end

@implementation OrderDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self localizeControls];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.preferredContentSize = CGSizeMake(350, 250);
    [self setCartItemValues];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self resetCartItemIfNeeded];
}

- (void)localizeControls {
    _lblQuantityTitle.text = [[ConfigurationHelper sharedInstance] appLocalizedStringForKey:@"QUANTITY"];
    _lblTotalTitle.text = [[ConfigurationHelper sharedInstance] appLocalizedStringForKey:@"TOTAL"];
    _lblPriceTitle.text = [[ConfigurationHelper sharedInstance] appLocalizedStringForKey:@"PRICE"];
    [_btnSave setTitle:[[ConfigurationHelper sharedInstance] appLocalizedStringForKey:@"ADD"] forState:UIControlStateNormal];
    [_btnCancel setTitle:[[ConfigurationHelper sharedInstance] appLocalizedStringForKey:@"CANCEL"] forState:UIControlStateNormal];
}

- (void)resetCartItemIfNeeded {
    // saving the added items
    if (_addedToCart) {
        NSInteger quantity = _lblQuantity.text.integerValue;
        if (!quantity) {
            // remove menu from cart if quantity is set to zero (removing item from cart)
            [[[TCCOrder sharedInstance] cartItems] removeObject:_cartItem];
        }
    }
    // the item should be removed if the quantity is the default one (new cart item);
    else if (_initialQuantity == TCCCartInitialValue) {
        [[[TCCOrder sharedInstance] cartItems] removeObject:_cartItem];
    }
    else {
        // reset cart to initial quantity since its not saved (for existing cart item; rejecting modification)
        _cartItem.quantity = _initialQuantity;
    }
}

- (void)setCartItemValues {
    _lblTitle.text = _menuItem.item;
    _lblPrice.text = [NSString stringWithFormat:@"%.2f", _menuItem.price.floatValue];
    _lblQuantity.text = [NSString stringWithFormat:@"%ld", (long)_cartItem.quantity];
    _lblTotal.text = [NSString stringWithFormat:@"%.2f", _cartItem.total];
    _stepper.value = _cartItem.quantity;
}

-(void)setMenuItem:(TCCMenuItem*)menuItem {
    _menuItem = menuItem;
    
    // get back the cart item to update the order for the item
    [[[TCCOrder sharedInstance] cartItems] enumerateObjectsUsingBlock:^(TCCCartItem *obj, NSUInteger idx, BOOL *stop) {
        if (obj.menuItem == _menuItem) {
            _cartItem = obj;
            *stop = YES;
        }
    }];
    if (!_cartItem) {
        _cartItem = [TCCCartItem new];
        _cartItem.menuItem = _menuItem;
        [[[TCCOrder sharedInstance] cartItems] addObject:_cartItem];
    }
    _initialQuantity = _cartItem.quantity;
}

#pragma mark - Button Actions

- (IBAction)quantityDidChange:(UIStepper*)sender {
    _cartItem.quantity = (int)sender.value;
    _lblQuantity.text = [NSString stringWithFormat:@"%ld", (long)_cartItem.quantity];
    _lblTotal.text = [NSString stringWithFormat:@"%.2f", _cartItem.total];
}

- (IBAction)btnSaveTapped:(id)sender {
    _addedToCart = YES;
    NSInteger quantity = _lblQuantity.text.integerValue;
    if (quantity) {
        _cartItem.quantity = _lblQuantity.text.integerValue;
        _cartItem.total = _lblTotal.text.floatValue;
    }
    else {
        // remove menu from cart if quantity is set to zero
        [[[TCCOrder sharedInstance] cartItems] removeObject:_cartItem];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)btnCancelTapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
