//
//  LanguageSelectionViewController.m
//  The Cavalry Club
//
//  Created by Ananth on 02/07/16.
//  Copyright © 2016 AvidMindWorks. All rights reserved.
//

#import "LanguageSelectionViewController.h"
#import "ConfigurationHelper.h"
#import "TCCAPICommunicator.h"

@interface LanguageSelectionViewController () <UIAlertViewDelegate>

@end

@implementation LanguageSelectionViewController

#pragma mark - Life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Button Actions

- (IBAction)languageSelected:(UIButton*)sender {
    [[ConfigurationHelper sharedInstance] setSelectedLanguage:(sender.tag % 1000)];
}

- (IBAction)syncBtnTapped:(id)sender {
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[[ConfigurationHelper sharedInstance] appLocalizedStringForKey:@"Alert"] message:[[ConfigurationHelper sharedInstance] appLocalizedStringForKey:@"Are you sure to sync ?"] delegate:self cancelButtonTitle:[[ConfigurationHelper sharedInstance] appLocalizedStringForKey:@"Cancel"] otherButtonTitles:[[ConfigurationHelper sharedInstance] appLocalizedStringForKey:@"Ok"], nil];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Are you sure to sync ?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
    alert.tag = kAlertView;
    [alert show];
}

#pragma mark <AlertView>

-(void)alertView:(UIAlertView*)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    [alertView dismissWithClickedButtonIndex:0 animated:NO];
    if (buttonIndex) {
        if (alertView.tag) {
            [[TCCAPICommunicator sharedInstance] syncRemoteData:^(NSError *error) {
                DLog(@"syncRemoteData completed");
            }];
        }
    }
}

@end
