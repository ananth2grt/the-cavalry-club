//
//  MenuItemsListViewController.m
//  The Cavalry Club
//
//  Created by Ananth on 02/07/16.
//  Copyright © 2016 AvidMindWorks. All rights reserved.
//

#import "MenuItemsListViewController.h"
#import "MenuDetailViewController.h"
#import "MenuItemCell.h"

static NSString *const itemDetailSegue = @"itemDetailSegue";

@implementation MenuItemsListViewController

#pragma mark - Life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.collectionView reloadData];
    });
}

-(void)setMenuItems:(NSArray*)menuItems {
    _menuItems = menuItems;
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)collectionView:(UICollectionView*)collectionView numberOfItemsInSection:(NSInteger)section {
    DLog(@"_menus.count: %d", _menuItems.count);
    return _menuItems.count;
}

- (UICollectionViewCell*)collectionView:(UICollectionView*)collectionView cellForItemAtIndexPath:(NSIndexPath*)indexPath {
    MenuItemCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[MenuItemCell cellID] forIndexPath:indexPath];
    cell.menuItem = _menuItems[indexPath.row];
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath*)indexPath {
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    [self performSegueWithIdentifier:itemDetailSegue sender:@(indexPath.row)];
}

#pragma mark <Navigation>

-(void)prepareForSegue:(UIStoryboardSegue*)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:itemDetailSegue]) {
        MenuDetailViewController *menuDetailViewController = (MenuDetailViewController*)segue.destinationViewController;
        menuDetailViewController.menuItem = _menuItems[[sender integerValue]];
    }
}

@end
