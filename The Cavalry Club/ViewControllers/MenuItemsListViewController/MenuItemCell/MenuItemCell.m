//
//  MenuItemCell.m
//  The Cavalry Club
//
//  Created by Ananth on 03/07/16.
//  Copyright © 2016 AvidMindWorks. All rights reserved.
//

#import "MenuItemCell.h"
#import "CacheImageView.h"
#import "TCCMenuItem.h"

#import "OrderDetailViewController.h"

@interface MenuItemCell () <UIPopoverPresentationControllerDelegate>
@property (weak, nonatomic) IBOutlet CacheImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *lblPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@end

@implementation MenuItemCell

+(NSString*)cellID {
    return @"MenuItemCell";
}

-(void)setMenuItem:(TCCMenuItem*)menuItem {
    _menuItem = menuItem;
    _lblTitle.text = _menuItem.item;
    _imageView.remotePath = [imagePathAPI stringByAppendingPathComponent:_menuItem.imagePath];
    _lblPrice.font = [UIFont fontWithName:[[ConfigurationHelper sharedInstance] appFont] size:19.0];
    _lblTitle.font = [UIFont fontWithName:[[ConfigurationHelper sharedInstance] appFont] size:19.0];
    _lblPrice.text = [NSString stringWithFormat:@"%@ %.2f", currencySymbol, _menuItem.price.floatValue];
    _lblTitle.textAlignment = [[ConfigurationHelper sharedInstance] selectedLanguage] == AppLanguageArabic ? NSTextAlignmentRight : NSTextAlignmentLeft;
}

- (IBAction)btnCartTapped:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    OrderDetailViewController * orderDetailViewController = [storyboard instantiateViewControllerWithIdentifier:@"OrderDetailViewController"];
    orderDetailViewController.menuItem = _menuItem;

    UIPopoverPresentationController *popPC = orderDetailViewController.popoverPresentationController;
    popPC.delegate = self;

    orderDetailViewController.modalPresentationStyle = UIModalPresentationFormSheet;
    [self.window.rootViewController presentViewController:orderDetailViewController animated:YES completion:nil];
}

@end
