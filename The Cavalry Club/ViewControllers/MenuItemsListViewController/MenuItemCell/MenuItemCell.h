//
//  MenuItemCell.h
//  The Cavalry Club
//
//  Created by Ananth on 03/07/16.
//  Copyright © 2016 AvidMindWorks. All rights reserved.
//

#import <UIKit/UIKit.h>
@class TCCMenuItem;

@interface MenuItemCell : UICollectionViewCell
@property (nonatomic, strong) TCCMenuItem *menuItem;
+ (NSString*)cellID;
@end
