//
//  TCCCollectionViewController.h
//  The Cavalry Club
//
//  Created by Ananth on 16/07/16.
//  Copyright © 2016 AvidMindWorks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TCCCollectionViewController : UICollectionViewController
- (void)scrollDown;
- (void)scrollUp;
@end
