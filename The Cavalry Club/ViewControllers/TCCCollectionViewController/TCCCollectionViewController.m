//
//  TCCCollectionViewController.m
//  The Cavalry Club
//
//  Created by Ananth on 16/07/16.
//  Copyright © 2016 AvidMindWorks. All rights reserved.
//

#import "TCCCollectionViewController.h"

@interface TCCCollectionViewController ()

@end

@implementation TCCCollectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (BOOL)canScroll {
    return self.collectionView.contentSize.height > self.view.frame.size.height;
}

-(void)scrollUp {
    if ([self canScroll]) {
        float newOffsetY = self.collectionView.contentOffset.y - self.view.frame.size.height;
        newOffsetY = newOffsetY < 0 ? : newOffsetY;
        [self.collectionView setContentOffset:CGPointMake(0, newOffsetY) animated:YES];
    }
}

-(void)scrollDown {
    if ([self canScroll]) {
        float newOffsetY = self.collectionView.contentOffset.y + self.view.frame.size.height;
        float maxOffsetY = self.collectionView.contentSize.height - self.view.frame.size.height;
        newOffsetY = newOffsetY < maxOffsetY ? newOffsetY : maxOffsetY;
        [self.collectionView setContentOffset:CGPointMake(0, newOffsetY) animated:YES];
    }
}

@end
