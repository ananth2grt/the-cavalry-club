//
//  BaseViewController.m
//  The Cavalry Club
//
//  Created by Ananth on 02/07/16.
//  Copyright © 2016 AvidMindWorks. All rights reserved.
//

#import "BaseViewController.h"
#import "CategoryCell.h"
#import "PromotionsViewController.h"
#import "MenuItemsListViewController.h"
#import "PromotionsDisplayViewController.h"
#import "OrderSummaryViewController.h"
#import "MenuCell.h"
#import "ReceptionViewController.h"

#import "TCCPromotion.h"
#import "TCCCategory.h"
#import "TCCMenuItem.h"
#import "TCCDisplayText.h"
#import "TCCAPICommunicator.h"
#import "UIView+AutoLayoutHelper.h"
#import "UIViewController+Docking.h"
#import "NSString+Helper.h"

static NSString *const promotionSegue = @"promotionSegue";
static NSString *const receptionSegue = @"receptionSegue";
static NSString *const beverageListSegue = @"beverageListSegue";
static NSString *const foodListSegue = @"foodListSegue";
static NSString *const orderSummarySegue = @"orderSummarySegue";
static NSString *const beveragePromotionDisplaySegue = @"beveragePromotionDisplaySegue";
static NSString *const foodPromotionDisplaySegue = @"foodPromotionDisplaySegue";

@interface BaseViewController () <OrderSummaryDelegate> {
    NSInteger _beveragesCategoryID, _foodsCategoryID;
    UIViewController *_currentContent;
}
@property (weak, nonatomic) IBOutlet UIView *childDock;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSArray *categories;
@property (nonatomic, strong) NSArray *promotions, *foodPromotions, *beveragePromotions;
@property (nonatomic, strong) NSArray *beverages, *uniqueBeverages;
@property (nonatomic, strong) NSArray *foods, *uniqueFoods;

@property (nonatomic, strong) NSArray *tableData;
@property (nonatomic, strong) NSArray *mutableMenuIDs;
@property (nonatomic, strong) NSIndexPath * openedIndexPath;
@end

@implementation BaseViewController

#pragma mark - Life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self populateMenuItems];
    [self getRemoteValues];
    DLog(@"%@", [NSString documentsDirectory]);
    _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self performSegueWithIdentifier:receptionSegue sender:@(TCCDisplayTextScreenWelcome)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [_tableView reloadData];
}

- (NSArray*)filterMenuItemsByCategory:(NSArray*)totalArray {
    NSMutableArray *uniqueMenuItems = [NSMutableArray array];
    __block TCCMenuItem *menuItemAll = nil;
    NSString *categoryNameALL = [[ConfigurationHelper sharedInstance] appLocalizedStringForKey:@"ALL"];
    [[totalArray valueForKeyPath:@"@distinctUnionOfObjects.category"] enumerateObjectsUsingBlock:^(id category, NSUInteger idx, BOOL *stop) {
        NSArray *uniqueBeverage = [totalArray filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(TCCMenuItem *menuItem, NSDictionary *bindings) {
            if ([menuItem.category isEqualToString:categoryNameALL]) {
                menuItemAll = menuItem;
            }
            return [menuItem.category isEqualToString:category];
        }]];
        if (uniqueBeverage.count > 0)
            [uniqueMenuItems addObject:uniqueBeverage.lastObject];
    }];
    NSInteger menuAllIDX = [uniqueMenuItems indexOfObject:menuItemAll];
    [uniqueMenuItems exchangeObjectAtIndex:menuAllIDX withObjectAtIndex:0];
    return [NSArray arrayWithArray:uniqueMenuItems];
}

- (void)getRemoteValues {
    __block typeof(self) weakSelf = self;
    [[TCCAPICommunicator sharedInstance] getPromotionsOfType:TCCPromotionTypeGeneral completion:^(NSArray *response, NSError *error) {
        weakSelf.promotions = response;
    }];
    [[TCCAPICommunicator sharedInstance] getPromotionsOfType:TCCPromotionTypeFood completion:^(NSArray *response, NSError *error) {
        if (!error) {
            weakSelf.foodPromotions = response;
        }
    }];
    [[TCCAPICommunicator sharedInstance] getPromotionsOfType:TCCPromotionTypeBeverage completion:^(NSArray *response, NSError *error) {
        if (!error) {
            weakSelf.beveragePromotions = response;
        }
    }];

    [[TCCAPICommunicator sharedInstance] getBeverages:^(NSArray *response, NSError *error) {
        if (!error) {
            weakSelf.beverages = response;
            weakSelf.uniqueBeverages = [weakSelf filterMenuItemsByCategory:response];
        }
    }];
    [[TCCAPICommunicator sharedInstance] getFoods:^(NSArray *response, NSError *error) {
        if (!error) {
            weakSelf.foods = response;
            weakSelf.uniqueFoods = [weakSelf filterMenuItemsByCategory:response];
        }
    }];
}

- (NSArray*)statusEnabledPromotions {
    NSMutableArray *statusEnabledPromotions = [NSMutableArray array];
    [_promotions enumerateObjectsUsingBlock:^(TCCPromotion *obj, NSUInteger idx, BOOL *stop) {
        if (obj.status) {
            [statusEnabledPromotions addObject:obj];
        }
    }];
    return statusEnabledPromotions;
}

#pragma mark - Button actions

- (IBAction)btnUpTapped:(id)sender {
    if ([_currentContent isKindOfClass:[TCCCollectionViewController class]]) {
        [(TCCCollectionViewController*)_currentContent scrollUp];
    }
}

- (IBAction)btnLogoTapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnDownTapped:(id)sender {
    if ([_currentContent isKindOfClass:[TCCCollectionViewController class]]) {
        [(TCCCollectionViewController*)_currentContent scrollDown];
    }
}

#pragma mark - Other methods

- (void)populateMenuItems {
    if (!_categories) {
        NSString *beverage = [[ConfigurationHelper sharedInstance] appLocalizedStringForKey:@"BEVERAGE"];
        NSString *food = [[ConfigurationHelper sharedInstance] appLocalizedStringForKey:@"FOOD"];
        NSMutableArray *categoryList = [NSMutableArray array];
        NSArray *menuList = @[[[ConfigurationHelper sharedInstance] appLocalizedStringForKey:@"ORDER SUMMARY"],
                               beverage, food,
                               [[ConfigurationHelper sharedInstance] appLocalizedStringForKey:@"PROMOTIONS"]];
        _mutableMenuIDs = @[@([menuList indexOfObject:beverage]),
                            @([menuList indexOfObject:food])];
        [menuList enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            TCCCategory *category = [TCCCategory new];
            category.hasChildren = [_mutableMenuIDs containsObject:@(idx)];
            if ([obj isEqualToString:beverage]) {
                _beveragesCategoryID = idx;
            }
            else if ([obj isEqualToString:food]) {
                _foodsCategoryID = idx;
            }
            category.name = obj;
            category.ID = idx;
            [categoryList addObject:category];
        }];
        _categories = [NSArray arrayWithArray:categoryList];
    }
    _tableData = [NSArray arrayWithArray:_categories];
}

#pragma mark - Table view delegates

-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath {
    id currentModel = _tableData[indexPath.row];
    if ([currentModel isKindOfClass:[TCCCategory class]]) {
        CategoryCell *cell = [tableView dequeueReusableCellWithIdentifier:[CategoryCell cellID] forIndexPath:indexPath];
        cell.category = currentModel;
        return cell;
    }
    else if ([currentModel isKindOfClass:[TCCMenuItem class]]) {
        MenuCell *cell = [tableView dequeueReusableCellWithIdentifier:[MenuCell cellID] forIndexPath:indexPath];
        cell.menuItem = currentModel;
        return cell;
    }
    return nil;
}

-(NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section {
    return _tableData.count;
}

-(void)tableView:(UITableView*)tableView didSelectRowAtIndexPath:(NSIndexPath*)indexPath {
    if ([self canExpandAt:indexPath]) {
        id currentModel = _tableData[indexPath.row];
        if (_openedIndexPath) {
            [currentModel setIsOpen:NO];
            [self collapseCategory:_openedIndexPath complete:nil];
        }
        else {
            // expand the beverage's category, as well as show promotion items with status '1'
            [currentModel setIsOpen:YES];
            [self expandCategory:indexPath complete:nil];
            if (indexPath.row == _beveragesCategoryID) {
                [self performSegueWithIdentifier:beveragePromotionDisplaySegue sender:self];
            }
            else if (indexPath.row == _foodsCategoryID) {
                [self performSegueWithIdentifier:foodPromotionDisplaySegue sender:self];
            }
        }
    }
    else {
        NSString *destinationSegue = nil;
        id currentModel = _tableData[indexPath.row];
        if ([currentModel isKindOfClass:[TCCCategory class]]) {
            static NSArray *segues = nil;
            if (!segues) {
                segues = @[orderSummarySegue, beverageListSegue, foodListSegue, promotionSegue];
            }
            destinationSegue = segues[[currentModel ID]];
        }
        else if ([currentModel isKindOfClass:[TCCMenuItem class]]) {
            destinationSegue = [currentModel menuType] == MenuTypeFood ? foodListSegue : beverageListSegue;
        }
        [self performSegueWithIdentifier:destinationSegue sender:currentModel];
    }
}

#pragma mark - Table view helpers

- (BOOL)canExpandAt:(NSIndexPath*)indexPath {
    // check for categories while expanding
    BOOL canExpand = [[_tableData objectAtIndex:indexPath.row] isKindOfClass:[TCCCategory class]];
    // check for children
    if (canExpand) {
        TCCCategory *category = (TCCCategory*)_tableData[indexPath.row];
        canExpand = category.hasChildren;
    }
    return canExpand;
}

- (void)expandCategory:(NSIndexPath*)indexPath complete:(void(^)())completionBlock; {
    
    if (![self canExpandAt:indexPath]) {
        if (completionBlock) {
            completionBlock();
        }
        return;
    }
    NSMutableArray *indexPathsToInsert = [NSMutableArray array];
    TCCCategory *category = (TCCCategory*)_tableData[indexPath.row];
    NSArray * children = (category.ID == _beveragesCategoryID) ? _uniqueBeverages : _uniqueFoods;
    
    // return if no children
    if (!children.count) {
        return;
    }
    [children enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSInteger insertingRow = indexPath.row + 1 + idx;
        [indexPathsToInsert addObject:[NSIndexPath indexPathForItem:insertingRow inSection:0]];
    }];
    
    // update table data
    NSIndexSet *indexes = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(indexPath.row + 1, children.count)];
    NSMutableArray * updatedTableData = [NSMutableArray arrayWithArray:_tableData];
    [updatedTableData insertObjects:children atIndexes:indexes];
    _tableData = [updatedTableData copy];
    
    [_tableView beginUpdates];
    [_tableView insertRowsAtIndexPaths:indexPathsToInsert withRowAnimation:UITableViewRowAnimationNone];
    [_tableView endUpdates];
    
    // update opened model indexPath
    _openedIndexPath = indexPath;
    if (completionBlock) {
        completionBlock();
    }
}

- (void)collapseCategory:(NSIndexPath*)indexPath complete:(void(^)())completionBlock; {
    // collapse only the opened indexpath
    if (indexPath.row != _openedIndexPath.row) {
        return;
    }
    NSInteger parentIDX = _openedIndexPath.row;
    TCCCategory *category = _tableData[indexPath.row];
    NSMutableArray * indexPathsToDelete = [NSMutableArray array];
    NSArray * children = (category.ID == _beveragesCategoryID) ? _uniqueBeverages : _uniqueFoods;
    
    [children enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSInteger deletingRow = parentIDX + 1 + idx;
        [indexPathsToDelete addObject:[NSIndexPath indexPathForItem:deletingRow inSection:0]];
    }];
    
    // update table data
    NSIndexSet *indexes = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(parentIDX + 1, children.count)];
    NSMutableArray * updatedTableData = [NSMutableArray arrayWithArray:_tableData];
    [updatedTableData removeObjectsAtIndexes:indexes];
    _tableData = [updatedTableData copy];
    
    [_tableView beginUpdates];
    [_tableView deleteRowsAtIndexPaths:indexPathsToDelete withRowAnimation:UITableViewRowAnimationNone];
    [_tableView endUpdates];
    
    // update opened model indexPath
    _openedIndexPath = nil;
    
    if (completionBlock) {
        completionBlock();
    }
}

#pragma mark - Segue Action For Navigation

- (NSArray*)itemsWithSameCategory:(NSArray*)items item:(TCCMenuItem*)item {
    NSMutableArray *itemsWithSameName = [NSMutableArray array];
    [items enumerateObjectsUsingBlock:^(TCCMenuItem *obj, NSUInteger idx, BOOL *stop) {
        if ([obj.category isEqualToString:item.category]) {
            [itemsWithSameName addObject:obj];
        }
    }];
    return itemsWithSameName;
}

-(void)prepareForSegue:(UIStoryboardSegue*)segue sender:(id)sender {
// remove previously added child to get the transparent background image of the self view
    if (_currentContent) {
        [_currentContent removeFromDock];
    }
    if ([segue.identifier isEqualToString:promotionSegue]) {
        PromotionsViewController *promotionsViewController = (PromotionsViewController*)segue.destinationViewController;
        promotionsViewController.promotions = _promotions;
        _currentContent = promotionsViewController;
    }
    else if ([segue.identifier isEqualToString:foodListSegue]) {
        MenuItemsListViewController *foodListViewController = (MenuItemsListViewController*)segue.destinationViewController;
        NSInteger currentModelIDX = [sender ID];
        NSMutableArray *foods = [_foods mutableCopy];
        [foods removeObjectAtIndex:0];

        if (currentModelIDX != MenuItemAllIDX) {
            // get items of same name
            foods = [[self itemsWithSameCategory:foods item:(TCCMenuItem*)sender] mutableCopy];
        }
        foodListViewController.menuItems = foods;
        _currentContent = foodListViewController;
    }
    else if ([segue.identifier isEqualToString:beverageListSegue]) {
        MenuItemsListViewController *beveragesListViewController = (MenuItemsListViewController*)segue.destinationViewController;
        NSInteger currentModelIDX = [sender ID];
        NSMutableArray *beverages = [_beverages mutableCopy];
        [beverages removeObjectAtIndex:0];
        
        if (currentModelIDX != MenuItemAllIDX) {
            // get items of same name 
            beverages = [[self itemsWithSameCategory:beverages item:(TCCMenuItem*)sender] mutableCopy];
        }
        beveragesListViewController.menuItems = beverages;
        _currentContent = beveragesListViewController;
    }
    else if ([segue.identifier isEqualToString:beveragePromotionDisplaySegue]) {
        PromotionsDisplayViewController *promotionsDisplayViewController = (PromotionsDisplayViewController*)segue.destinationViewController;
        promotionsDisplayViewController.promotions = _beveragePromotions;
        _currentContent = promotionsDisplayViewController;
    }
    else if ([segue.identifier isEqualToString:foodPromotionDisplaySegue]) {
        PromotionsDisplayViewController *promotionsDisplayViewController = (PromotionsDisplayViewController*)segue.destinationViewController;
        promotionsDisplayViewController.promotions = _foodPromotions;
        _currentContent = promotionsDisplayViewController;
    }
    else if ([segue.identifier isEqualToString:orderSummarySegue]) {
        OrderSummaryViewController *orderSummaryViewController = (OrderSummaryViewController*)segue.destinationViewController;
        orderSummaryViewController.delegate = self;
        _currentContent = orderSummaryViewController;
    }
    else if ([segue.identifier isEqualToString:receptionSegue]) {
        ReceptionViewController *receptionViewController = (ReceptionViewController*)segue.destinationViewController;
        receptionViewController.displayTextScreenType = (TCCDisplayTextScreen)[sender integerValue];
        _currentContent = receptionViewController;
    }
    [self addChild:_currentContent toView:_childDock];
}

#pragma mark - <OrderSummaryDelegate>

-(void)didSelectReset:(OrderSummaryViewController*)sender {
    if (_currentContent == sender) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

@end
