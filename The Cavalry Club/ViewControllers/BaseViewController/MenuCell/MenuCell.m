//
//  MenuCell.m
//  The Cavalry Club
//
//  Created by Ananth on 07/07/16.
//  Copyright © 2016 AvidMindWorks. All rights reserved.
//

#import "MenuCell.h"
#import "TCCMenuItem.h"

@implementation MenuCell

+ (NSString*)cellID {
    return @"MenuCell";
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    self.contentView.backgroundColor = selected ? [UIColor whiteColor] : [UIColor colorWithRed:51/255.0 green:51/255.0 blue:51/255.0 alpha:.8];
    self.textLabel.textColor = selected ? [UIColor blackColor] : [UIColor whiteColor];
}

-(void)setMenuItem:(TCCMenuItem*)menuItem {
    _menuItem = menuItem;
    self.contentView.backgroundColor = [UIColor colorWithRed:51/255.0 green:51/255.0 blue:51/255.0 alpha:.8];
    self.textLabel.font = [UIFont fontWithName:[[ConfigurationHelper sharedInstance] appFont] size:16.0];
    self.backgroundColor = [UIColor clearColor];
    self.textLabel.textAlignment = [[ConfigurationHelper sharedInstance] selectedLanguage] == AppLanguageArabic ? NSTextAlignmentRight : NSTextAlignmentLeft;
    self.textLabel.text = _menuItem.category;
}

@end
