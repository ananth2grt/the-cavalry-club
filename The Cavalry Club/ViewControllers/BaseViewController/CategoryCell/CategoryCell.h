//
//  CategoryCell.h
//  The Cavalry Club
//
//  Created by Ananth on 02/07/16.
//  Copyright © 2016 AvidMindWorks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TCCCategory.h"
@interface CategoryCell : UITableViewCell
@property (nonatomic, strong) TCCCategory *category;
+(NSString*)cellID;
@end
