//
//  CategoryCell.m
//  The Cavalry Club
//
//  Created by Ananth on 02/07/16.
//  Copyright © 2016 AvidMindWorks. All rights reserved.
//

#import "CategoryCell.h"

@interface CategoryCell ()
@property (weak, nonatomic) IBOutlet UIImageView *arrowImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *bgImageView;
@end

@implementation CategoryCell

+ (NSString*)cellID {
    return @"CategoryCell";
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    self.contentView.backgroundColor = selected ? [UIColor whiteColor] : [UIColor colorWithRed:51/255.0 green:51/255.0 blue:51/255.0 alpha:.8];
    _titleLabel.textColor = selected ? [UIColor blackColor] : [UIColor whiteColor];
    if (_category.hasChildren) {
        NSString *imageName = selected ? (_category.isOpen ? @"up_arw_bk_icn" : @"down_arw_bk_icn")
        : (_category.isOpen ? @"up_arw_wt_icn" : @"down_arw_wt_icn");
        _arrowImageView.image = [UIImage imageNamed:imageName];
    }
}

-(void)setCategory:(TCCCategory*)category {
    _category = category;
    self.backgroundColor = [UIColor clearColor];
    _arrowImageView.hidden = _category.hasChildren ? NO : YES;
    self.contentView.backgroundColor = [UIColor colorWithRed:51/255.0 green:51/255.0 blue:51/255.0 alpha:.8];
    _titleLabel.textAlignment = [[ConfigurationHelper sharedInstance] selectedLanguage] == AppLanguageArabic ? NSTextAlignmentRight : NSTextAlignmentLeft;
    _titleLabel.font = [UIFont fontWithName:[[ConfigurationHelper sharedInstance] appFont] size:16.0];
    // not setting title for "Order summary"
    _titleLabel.text = _category.ID ? _category.name : @"";
    _bgImageView.hidden = _category.ID;
}

@end
