//
//  NSString+Helper.m
//  The Cavalry Club
//
//  Created by Ananth on 02/07/16.
//  Copyright © 2016 AvidMindWorks. All rights reserved.
//

#import "NSString+Helper.h"

@implementation NSString (Helper)

+ (NSString*)documentsDirectory {
    return NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, YES, YES)[0];
}

+ (NSString*)cachesDirectory {
    return NSSearchPathForDirectoriesInDomains(NSCachesDirectory, YES, YES)[0];
}

@end
