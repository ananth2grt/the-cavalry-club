//
//  NSString+Helper.h
//  The Cavalry Club
//
//  Created by Ananth on 02/07/16.
//  Copyright © 2016 AvidMindWorks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Helper)
+ (NSString*)documentsDirectory;
+ (NSString*)cachesDirectory;
@end
