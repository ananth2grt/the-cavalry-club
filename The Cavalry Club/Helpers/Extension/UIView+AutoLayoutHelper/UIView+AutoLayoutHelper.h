//
//  UIView+AutoLayoutHelper.h
//  The Cavalry Club
//
//  Created by Ananth on 02/07/16.
//  Copyright © 2016 AvidMindWorks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (AutoLayoutHelper)
- (void)fitView:(UIView*)aView;
@end
