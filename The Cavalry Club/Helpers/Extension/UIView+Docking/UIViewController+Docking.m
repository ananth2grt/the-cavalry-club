//
//  UIViewController+Docking.m
//  The Cavalry Club
//
//  Created by Ananth on 03/07/16.
//  Copyright © 2016 AvidMindWorks. All rights reserved.
//

#import "UIViewController+Docking.h"
#import "UIView+AutoLayoutHelper.h"

@implementation UIViewController (Docking)

-(void)addChild:(UIViewController*)aViewController toView:(UIView*)aView {
    UIView *dock = aView ? : self.view;
    [self addChildViewController:aViewController];
    [dock addSubview:aViewController.view];
    [dock fitView:aViewController.view];
    [aViewController didMoveToParentViewController:self];
}

-(void)removeFromDock {
    // self must be a child added to a parent view controller
    [self willMoveToParentViewController:nil];
    [self removeFromParentViewController];
    [self.view removeFromSuperview];
}

@end
