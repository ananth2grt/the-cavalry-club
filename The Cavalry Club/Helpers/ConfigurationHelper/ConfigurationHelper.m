//
//  ConfigurationHelper.m
//  The Cavalry Club
//
//  Created by Ananth on 02/07/16.
//  Copyright © 2016 AvidMindWorks. All rights reserved.
//

#import "ConfigurationHelper.h"

NSString *const promotionsGeneralAPI = @"http://www.goldentulipbahrainapp.com/api/promotion.php";
NSString *const promotionsBeverageAPI = @"http://goldentulipbahrainapp.com/api/beverage_prom.php";
NSString *const promotionsFoodAPI = @"http://goldentulipbahrainapp.com/api/food_prom.php";
NSString *const beverageAPI = @"http://www.goldentulipbahrainapp.com/api/beverage.php";
NSString *const foodAPI = @"http://www.goldentulipbahrainapp.com/api/food.php";
NSString *const imagePathAPI = @"http://www.goldentulipbahrainapp.com/";

NSString *const welcomeTextAPI = @"http://www.goldentulipbahrainapp.com/api/contentdashbord.php?id=1";
NSString *const foodTextAPI = @"http://www.goldentulipbahrainapp.com/api/contentdashbord.php?id=2";

NSString *const responseDataKey = @"title";
NSString *const currencySymbol = @"BD";

const NSInteger kAlertView = 1000;

@implementation ConfigurationHelper

+ (id)sharedInstance
{
    // structure used to test whether the block has completed or not
    static dispatch_once_t p = 0;
    
    // initialize sharedObject as nil (first call only)
    __strong static id _sharedObject = nil;
    
    // executes a block object once and only once for the lifetime of an application
    dispatch_once(&p, ^{
        _sharedObject = [[self alloc] init];
    });
    
    // returns the same object each time
    return _sharedObject;
}

-(void)setSelectedLanguage:(AppLanguage)selectedLanguage {
    _selectedLanguage = selectedLanguage;
    _appFont = _selectedLanguage == AppLanguageEnglish ? @"Abel-Regular" : @"GE_SS_Two_Light";
}


-(NSString*)appLocalizedStringForKey:(NSString*)key {
    NSString *path = [[NSBundle mainBundle] pathForResource:(([[ConfigurationHelper sharedInstance] selectedLanguage] == AppLanguageEnglish) ? @"Base" : @"ar") ofType:@"lproj"];
    return [[NSBundle bundleWithPath:path] localizedStringForKey:key value:nil table:nil];
}

@end
