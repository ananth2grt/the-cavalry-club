//
//  ConfigurationHelper.h
//  The Cavalry Club
//
//  Created by Ananth on 02/07/16.
//  Copyright © 2016 AvidMindWorks. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, AppLanguage) {
    AppLanguageEnglish,
    AppLanguageArabic
};

extern NSString *const promotionsGeneralAPI;
extern NSString *const promotionsBeverageAPI;
extern NSString *const promotionsFoodAPI;
extern NSString *const beverageAPI;
extern NSString *const foodAPI;
extern NSString *const imagePathAPI;
extern NSString *const welcomeTextAPI;
extern NSString *const foodTextAPI;
extern NSString *const responseDataKey;
extern NSString *const currencySymbol;
extern const NSInteger kAlertView;

@interface ConfigurationHelper : NSObject
@property (nonatomic) AppLanguage selectedLanguage;
@property (nonatomic, strong) NSString *appFont;
- (NSString*)appLocalizedStringForKey:(NSString*)key;
+ (id)sharedInstance;
@end
